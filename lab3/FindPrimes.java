public class FindPrimes {
    public static void main(String[] args) {
        int max =Integer.parseInt(args[0]);

        for(int number =2;number<max;number++){
            int divisior =2;

            boolean isPrime = true;

            while (divisior<number && isPrime) {
                if(number % divisior==0)
                    isPrime=false;
                divisior++;
            }
            if(isPrime)
                System.out.println(number+" ");
        }
    }
}
  